#!/usr/bin/env bash

CLUSTER_NAME="PRODUCTION"
TAG_NAME=kubespray_cluster
KUBECTL_VERSION=1.16.2

# Verify that doctl is configured for use.
doctl account get 1> /dev/null
if [[ $? != 0 ]]; then
  echo "This script relies on the digitalocean cli (doctl)"
  echo "Please install and configure doctl."
  echo "https://github.com/digitalocean/doctl#installing-doctl"
  exit 1
fi

# Check that the user has exported an SSH key fingerprint.
if [ -z ${DO_SSH_KEY+x} ]; then
  echo "Please set the environment variable DO_SSH_KEY to a public"
  echo "key fingerprint that has been uploaded to DigitalOcean."
  echo "https://www.digitalocean.com/docs/droplets/how-to/add-ssh-keys/to-account/"
  exit 1
fi

# Ensure that there are no existing nodes tagged for our cluster.
if [[ $(doctl compute droplet list --format "PublicIPv4" --no-header --tag-name "${TAG_NAME}" | wc -l) -gt 0 ]] ; then
  echo "Existing nodes found with the tag ${TAG_NAME}."
  echo "Delete them before continuing"
  exit 1
fi

if ! hash kubectl 2> /dev/null; then 
  echo "Installing kubectl ${KUBECTL_VERSION}"
  sudo curl -s -L -o /usr/local/bin/kubectl https://storage.googleapis.com/kubernetes-release/release/v${KUBECTL_VERSION}/bin/linux/${ARCH}/kubectl
  sudo chmod +x /usr/local/bin/kubectl

  if [[ ! -d /etc/bash_completion.d/ ]] ; then
    sudo mkdir -p /etc/bash_completion.d/
  fi
  kubectl completion bash | sudo tee -a /etc/bash_completion.d/kubectl > /dev/null

  if ! type _init_completion 2&>1 /dev/null ; then
    grep -qxF 'source /usr/share/bash-completion/bash_completion' ${HOME}/.bashrc || echo 'source /usr/share/bash-completion/bash_completion' >> ${HOME}/.bashrc      
  fi

  grep -qxF 'source <(kubectl completion bash)' ${HOME}/.bashrc || echo 'source <(kubectl completion bash)' >> ${HOME}/.bashrc
  . ${HOME}/.bashrc
else
  if [[ $(kubectl version --client --short) =~ ${KUBECTL_VERSION} ]] ; then
   echo "INFO: kubectl requirement ${KUBECTL_VERSION} satisfied."
  else
    echo "WARNING: kubectl $(kubectl version --client --short), expected ${KUBECTL_VERSION}."
  fi
fi



# Create our tag if it doesn't exist
doctl compute tag create ${TAG_NAME} 1> /dev/null

echo "Building digitalocean virtual machines..."
for node in kube-n01 kube-n02 kube-n03 kube-n04; do
	doctl compute droplet create ${node} \
		--size s-2vcpu-4gb \
		--image centos-7-x64 \
		--region nyc1 \
		--ssh-keys ${DO_SSH_KEY} \
		--enable-private-networking \
		--tag-name "${TAG_NAME}" \
    --format "Name,PublicIPv4,PrivateIPv4" \
		--wait
done

declare -a PUBLIC_IPS=($(doctl compute droplet list --format "PublicIPv4" --no-header --tag-name ${TAG_NAME} | tac))
declare -a PRIVATE_IPS=($(doctl compute droplet list --tag-name ${TAG_NAME} --no-header --format "PrivateIPv4" | tac))

#CONTROL_PLANE=$(echo "${PUBLIC_IPS}" | head -1)

# Update the DNS record for kube.homeskillet.org
echo "Updating kube.homeskillet.org DNS record to ${PUBLIC_IPS[0]}"
doctl compute domain records update homeskillet.org --record-id=89006212 --record-data="${PUBLIC_IPS[0]}" 1> /dev/null

echo "Verifying that there are exactly 4 nodes for our cluster..."
if [[ ${#PUBLIC_IPS[@]} -ne 4 ]] ; then
  "There are not exactly four nodes tagged for this cluster. exiting."
  exit 1
fi

echo "Creating ansible inventory at inventory/mycluster/inventory.ini"

# Create an ansible inventory file with our new servers.
cat > inventory/mycluster/inventory.ini <<EOL

[all]
kube-n01 ansible_host=${PUBLIC_IPS[0]} ip=${PRIVATE_IPS[0]}
kube-n02 ansible_host=${PUBLIC_IPS[1]} ip=${PRIVATE_IPS[1]} etcd_member_name=etcd1
kube-n03 ansible_host=${PUBLIC_IPS[2]} ip=${PRIVATE_IPS[2]} etcd_member_name=etcd2
kube-n04 ansible_host=${PUBLIC_IPS[3]} ip=${PRIVATE_IPS[3]} etcd_member_name=etcd3

# ## configure a bastion host if your nodes are not directly reachable
# bastion ansible_host=x.x.x.x ansible_user=some_user

[kube-master]
kube-n01
kube-n02

[etcd]
kube-n02
kube-n03
kube-n04

[kube-node]
kube-n01
kube-n02
kube-n03
kube-n04


[calico-rr]

[k8s-cluster:children]
kube-master
kube-node
calico-rr

[all:vars]
ansible_connection=ssh
ansible_ssh_user=root
ansible_ssh_common_args='-o StrictHostKeyChecking=no'
EOL
