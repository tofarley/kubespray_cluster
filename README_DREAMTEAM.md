# README_DREAMTEAM
This will create a four-node kubernetes cluster on DigitalOcean using kubespray.

## Prerequisites
Configure your local system or virtualmachine from which you plan to run the deployment.

The following will install `python3` and `python3-pip` system-wide and set them as default. You may prefer to install them into a virtual environment. That is left as an exercise to the user.

### Ubuntu/Debian
```
sudo apt update
sudo apt install -y git openssl curl python3 python3-pip bash-completion
sudo update-alternatives --install /usr/bin/python python /usr/bin/python3 10
sudo update-alternatives --install /usr/bin/pip pip /usr/bin/pip3 10
```

### RHEL/CentOS
```
sudo yum install -y git openssl curl python3 python3-pip bash-completion
sudo update-alternatives --install /usr/bin/python python /usr/bin/python3 10
sudo update-alternatives --install /usr/bin/pip pip /usr/bin/pip3 10
```

### DigitalOcean
Generate a DigitalOcean API token:
* Navigate in the DO portal to `Manage \ API \ Tokens/Keys`.
* Grant the token both read and write access.
* export your new token in your `~/.bashrc` file as `DO_API_KEY`.

Upload your SSH public key to DigitalOcean:
* Navigate in the DO portal to `Account \ Security`.
* Export the fingerprint of this newly added key in your `~/.bashrc` file as `DO_SSH_KEY`.

This key will be used for ssh access to each of your nodes, and for ansible provisioning.

### Node requirements
As of this writing, the following Linux distributions and Docker versions are supported.

* CentOS 7.5 / 7.6 / 7.7
* Docker 17.03.2 / 18.06.2 / 18.09.x / 19.03.x

*CentOS 8 is not yet supported.*

## Installation
Checkout a tagged release of kubespray and install requirements.
```
git clone https://github.com/kubernetes-sigs/kubespray.git
cd kubespray
git checkout tags/v2.12.1 -b v2.12.1
pip install -r requirements.txt
```

Merge in our custom settings from our `kubespray_cluster` repo.
```
git remote add kubespray_cluster https://gitlab.com/tofarley/kubespray_cluster.git
git fetch kubespray_cluster
git merge kubespray_cluster/master --allow-unrelated-histories
```

We have now merged in our custom `./inventory/mycluster/` directory that contains our custom settings for the clu ster, along with two new shell scripts: `build_environment.sh` and `destroy_environment.sh`.

This `build_environment.sh` script will create our DigitalOcean nodes, update our DNS records to point to the newly-created master node, and generate our ansible inventory file at `inventory/mycluster/hosts.yaml`.

```
./build_environment
```

Prepare our nodes with a few basic requirements, such as enabling ip-forwarding and required kernel modules.
```
ansible-playbook -i inventory/mycluster/inventory.ini configure_nodes.yml
```

Deploy our cluster!
```
ansible-playbook -i inventory/mycluster/inventory.ini cluster.yml
```

## Accessing the cluster
ssh into one of the master nodes (`kube-n01` or `kube-n02` by default) and retrive the contents of the file `/etc/kubernetes/admin.conf`.

Paste the contents of the file onto your local workstation into the file `${HOME}/.kube/config`.

You can now access the files with kubectl!